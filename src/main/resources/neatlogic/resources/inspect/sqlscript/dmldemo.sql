BEGIN;


insert  into `inspect_alert_everyday`(`report_time`,`resource_id`,`alert_level`,`alert_object`,`alert_rule`,`alert_tips`,`alert_value`) values ('2023-06-05',558468884865029,'CRITICAL','AVAILABILITY(可用性)\r\n','$.AVAILABILITY != True','可用性告警','false');
insert  into `inspect_alert_everyday`(`report_time`,`resource_id`,`alert_level`,`alert_object`,`alert_rule`,`alert_tips`,`alert_value`) values ('2023-06-05',558468884865029,'CRITICAL','ERROR_MESSAGE(错误信息)\r\n','$.ERROR_MESSAGE != \"\"','错误信息非空','\'ERROR: Connect to 192.168.0.35:0 failed, [Errno 111] Connection refused.\'');
insert  into `inspect_alert_everyday`(`report_time`,`resource_id`,`alert_level`,`alert_object`,`alert_rule`,`alert_tips`,`alert_value`) values ('2023-06-05',558582651166720,'CRITICAL','AVAILABILITY(可用性)\r\n','$.AVAILABILITY != True','可用性告警','false');
insert  into `inspect_alert_everyday`(`report_time`,`resource_id`,`alert_level`,`alert_object`,`alert_rule`,`alert_tips`,`alert_value`) values ('2023-06-05',558582651166720,'CRITICAL','ERROR_MESSAGE(错误信息)\r\n','$.ERROR_MESSAGE != \"\"','错误信息非空','\'ERROR: Connect to 192.168.0.16:0 failed, timed out.\'');





insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441090540838912,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441099676033024,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441108425351168,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441108962222080,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441109692030976,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441111420084224,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441112812593152,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441114163159040,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441115220123648,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441127224221696,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441127945641984,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441128742559744,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441130680328192,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441131519188992,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441132332883968,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441154319425536,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441155468664832,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441157381267456,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441157926526976,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441158580838400,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441183134294016,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441183880880128,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441184585523200,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441185139171328,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441186070306816,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441189685796864,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441190407217152,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441190910533632,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441191917166592,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441192521146368,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441193116737536,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441193880100864,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441194626686976,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (441900016345088,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (442011534499840,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (478816686317568,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (479593471418368,651192749268992);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (479596491317248,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (479598143873024,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (479603630022656,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (479606037553152,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (480157781467136,651192749268992);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (482443215560704,651192749268992);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (482443702099968,651192749268992);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (482443978924032,651192749268992);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (482994858811392,651225204793344);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (482995731226624,651226370809856);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (483000076525568,651225808773120);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (483270063874052,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (483276229500928,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (483279895322624,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (483287419904000,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (483300590018560,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (493301966839808,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (496318208286720,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (546978471624704,651220607836160);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (547538604138496,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (573130988855296,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (573132951789568,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (573133958422528,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (573134277189632,NULL);
insert  into `inspect_ci_combop`(`ci_id`,`combop_id`) values (594812604522496,NULL);
COMMIT;